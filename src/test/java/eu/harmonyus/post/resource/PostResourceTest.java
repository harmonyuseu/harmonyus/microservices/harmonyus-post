package eu.harmonyus.post.resource;

import eu.harmonyus.post.model.PostEntity;
import eu.harmonyus.post.resource.model.Post;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.vertx.core.http.HttpHeaders;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
class PostResourceTest {

    @ConfigProperty(name = "quarkus.resteasy.path")
    String restPath;

    @BeforeEach
    void setup() {
        PostEntity.deleteAll().await().indefinitely();
    }

    @Test
    @DisplayName("Test - When Calling POST - /api/{version}/posts should create resource - 201")
    void testCreatePost() {
        Post post = createPost();

        given()
                .when()
                .body(post)
                .contentType(ContentType.JSON)
                .post(restPath + "/posts")
                .then()
                .statusCode(201)
                .header(HttpHeaders.LOCATION.toString(), containsString(restPath + "/posts/post-number-one"))
                .body("title", is("Post number one"))
                .body("body", is("Body of the this post"))
                .body("code", is("post-number-one"))
                .body("author.url", is("https://author.harmonyus.eu/authors/john-mathew"));
    }

    @Test
    @DisplayName("Test - When Calling POST - /api/{version}/posts should create resource - 201")
    void testCreatePostWithCategoryAndTags() {
        Post post = createPost();

        given()
                .when()
                .body(post)
                .contentType(ContentType.JSON)
                .post(restPath + "/posts")
                .then()
                .statusCode(201)
                .header(HttpHeaders.LOCATION.toString(), containsString(restPath + "/posts/post-number-one"))
                .body("title", is("Post number one"))
                .body("body", is("Body of the this post"))
                .body("code", is("post-number-one"))
                .body("author.url", is("https://author.harmonyus.eu/authors/john-mathew"))
                .body("category.url", is("https://category.harmonyus.eu/categories/category-one"))
                .body("tags[0].url", is("https://tag.harmonyus.eu/tags/tag-one"))
                .body("tags[1].url", is("https://tag.harmonyus.eu/tags/tag-two"))
                .body("tags[2].url", is("https://tag.harmonyus.eu/tags/tag-three"));
    }

    @Test
    @DisplayName("Test - When Calling DELETE - /api/{version}/posts/{id} should delete resource - 204")
    void testDeletePost() {
        Post post = createPost();

        given()
                .when()
                .body(post)
                .contentType(ContentType.JSON)
                .post(restPath + "/posts");

        given()
                .when()
                .get(restPath + "/posts/post-number-one")
                .then()
                .statusCode(200);

        given()
                .when()
                .delete(restPath + "/posts/post-number-one")
                .then()
                .statusCode(204);

        given()
                .when()
                .get(restPath + "/posts/post-number-one")
                .then()
                .statusCode(404);
    }

    @Test
    @DisplayName("Test - When Calling PUT - /api/{version}/posts/{id} should update resource - 200")
    void testUpdatePost() {
        Post post = createPostWithTypo();

        given()
                .when()
                .body(post)
                .contentType(ContentType.JSON)
                .post(restPath + "/posts");

        post = createPost();
        post.code = "post-number-one";

        given()
                .when()
                .body(post)
                .contentType(ContentType.JSON)
                .put(restPath + "/posts/ost-number-one")
                .then()
                .statusCode(200);

        given()
                .when()
                .get(restPath + "/posts/post-number-one")
                .then()
                .statusCode(200)
                .body("title", is("Post number one"))
                .body("body", is("Body of the this post"))
                .body("code", is("post-number-one"))
                .body("author.url", is("https://author.harmonyus.eu/authors/john-mathew"))
                .body("category.url", is("https://category.harmonyus.eu/categories/category-one"))
                .body("tags[0].url", is("https://tag.harmonyus.eu/tags/tag-one"))
                .body("tags[1].url", is("https://tag.harmonyus.eu/tags/tag-two"))
                .body("tags[2].url", is("https://tag.harmonyus.eu/tags/tag-three"));
    }

    @Test
    @DisplayName("Test - When Calling GET - /api/{version}/posts should return resources - 200")
    void testGetAll() {
        createPostEntity("Post number one");
        createPostEntity("Post number two");
        createPostEntity("Post number three");

        Post[] posts =  given()
                .when()
                .get(restPath + "/posts")
                .then()
                .statusCode(200)
                .extract()
                .as(Post[].class);
        assertThat("Posts length", posts.length, equalTo(3));
        assertPostValue(posts[2], "Post number one", "post-number-one");
        assertPostValue(posts[1], "Post number two", "post-number-two");
        assertPostValue(posts[0], "Post number three", "post-number-three");
    }

    @Test
    @DisplayName("Test - When Calling GET - /api/{version}/posts should return resources - 200")
    void testGetAllByAuthor() {
        createPostEntity("Post number one");
        createPostEntity("Post number two");
        createPostEntity("Post number three");

        Post[] posts =  given()
                .when()
                .get(restPath + "/posts?authorCode=john-mathew")
                .then()
                .statusCode(200)
                .extract()
                .as(Post[].class);
        assertThat("Posts length", posts.length, equalTo(3));
        assertPostValue(posts[2], "Post number one", "post-number-one");
        assertPostValue(posts[1], "Post number two", "post-number-two");
        assertPostValue(posts[0], "Post number three", "post-number-three");
    }

    @Test
    @DisplayName("Test - When Calling GET - /api/{version}/posts should return resources - 200")
    void testGetAllByCategory() {
        createPostEntity("Post number one");
        createPostEntity("Post number two");
        createPostEntity("Post number three");

        Post[] posts =  given()
                .when()
                .get(restPath + "/posts?categoryCode=category-one")
                .then()
                .statusCode(200)
                .extract()
                .as(Post[].class);
        assertThat("Posts length", posts.length, equalTo(3));
        assertPostValue(posts[2], "Post number one", "post-number-one");
        assertPostValue(posts[1], "Post number two", "post-number-two");
        assertPostValue(posts[0], "Post number three", "post-number-three");
    }

    @Test
    @DisplayName("Test - When Calling GET - /api/{version}/posts should return resources - 200")
    void testGetAllByTag() {
        createPostEntity("Post number one");
        createPostEntity("Post number two");
        createPostEntity("Post number three");

        Post[] posts =  given()
                .when()
                .get(restPath + "/posts?tagCode=tag-one")
                .then()
                .statusCode(200)
                .extract()
                .as(Post[].class);
        assertThat("Posts length", posts.length, equalTo(3));
        assertPostValue(posts[2], "Post number one", "post-number-one");
        assertPostValue(posts[1], "Post number two", "post-number-two");
        assertPostValue(posts[0], "Post number three", "post-number-three");
    }

    private void assertPostValue(Post post, String title, String code) {
        assertThat(post.title, equalTo(title));
        assertThat(post.code, equalTo(code));
        assertThat(post.body, equalTo("Body of the this post"));
        assertThat(post.author.getCode(), equalTo("john-mathew"));
        assertThat(post.category.getCode(), equalTo("category-one"));
        assertThat(post.tags.get(0).getCode(), equalTo("tag-one"));
        assertThat(post.tags.get(1).getCode(), equalTo("tag-two"));
        assertThat(post.tags.get(2).getCode(), equalTo("tag-three"));
    }

    private Post createPost() {
        Post post = new Post();
        post.title = "Post number one";
        post.body = "Body of the this post";
        post.author = createAuthorCode();
        post.category = createCategoryCode();
        post.tags = Arrays.asList(
                createTag("tag-one"),
                createTag("tag-two"),
                createTag("tag-three")
        );
        return post;
    }

    private Post createPostWithTypo() {
        Post post = createPost();
        post.title = "ost number one";
        post.body = "ody of the this post";
        post.author.url = "https://author.harmonyus.eu/authors/ohn-mathew";
        post.category.url = "https://category.harmonyus.eu/categories/ategory-one";
        post.tags = Arrays.asList(
                createTag("https://tag.harmonyus.eu/tags/ag-one"),
                createTag("https://tag.harmonyus.eu/tags/ag-two"),
                createTag("https://tag.harmonyus.eu/tags/ag-three")
        );
        return post;
    }

    private Post.UrlCode createAuthorCode() {
        return new Post.UrlCode("https://author.harmonyus.eu/authors/john-mathew");
    }

    private Post.UrlCode createCategoryCode() {
        return new Post.UrlCode("https://category.harmonyus.eu/categories/category-one");
    }

    private Post.UrlCode createTag(String code) {
        return new Post.UrlCode("https://tag.harmonyus.eu/tags/" + code);
    }

    private void createPostEntity(String title) {
        PostEntity postEntity = new PostEntity();
        postEntity.setTitle(title);
        postEntity.setBody("Body of the this post");
        postEntity.setAuthorCode("john-mathew");
        postEntity.setCategoryCode("category-one");
        postEntity.setTagsCode(Arrays.asList("tag-one", "tag-two", "tag-three"));
        postEntity.persist().await().indefinitely();
    }
}