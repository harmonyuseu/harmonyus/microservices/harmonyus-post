package eu.harmonyus.post.model;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoEntityBase;
import io.smallrye.mutiny.Uni;
import org.bson.codecs.pojo.annotations.BsonId;

import java.text.Normalizer;
import java.time.LocalDateTime;
import java.util.List;

@MongoEntity(collection = "post")
public class PostEntity extends ReactivePanacheMongoEntityBase {

    @BsonId
    private String code;
    private LocalDateTime creation = null;
    private LocalDateTime modification = null;

    private String title;
    private String body;
    private String authorCode;
    private String categoryCode;
    private List<String> tagsCode;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getAuthorCode() {
        return authorCode;
    }

    public void setAuthorCode(String authorCode) {
        this.authorCode = authorCode;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public List<String> getTagsCode() {
        return tagsCode;
    }

    public void setTagsCode(List<String> tagsCode) {
        this.tagsCode = tagsCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getCreation() {
        return creation;
    }

    public void setCreation(LocalDateTime creation) {
        this.creation = creation;
    }

    public LocalDateTime getModification() {
        return modification;
    }

    public void setModification(LocalDateTime modification) {
        this.modification = modification;
    }

    @Override
    public Uni<Void> persist() {
        if (this.code == null) {
            this.code = createCode(title);
        }
        if (creation == null) {
            creation = LocalDateTime.now();
        }
        return super.persist();
    }

    @Override
    public Uni<Void> persistOrUpdate() {
        if (modification == null) {
            modification = LocalDateTime.now();
        }
        return super.persistOrUpdate();
    }

    private static String createCode(String value) {
        return stripAccents(value.toLowerCase()).replace(" ", "-").replace("'", "-");
    }

    private static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }
}
