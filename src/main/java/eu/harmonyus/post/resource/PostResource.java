package eu.harmonyus.post.resource;

import eu.harmonyus.post.model.PostEntity;
import eu.harmonyus.post.resource.mapper.PostMapper;
import eu.harmonyus.post.resource.model.Post;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoEntityBase;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.time.Duration;
import java.util.List;

@Path("/posts")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PostResource {

    @ConfigProperty(name = "harmonyus.post-resource.get-all.timeout", defaultValue = "2s")
    Duration getAllTimeout;
    @ConfigProperty(name = "harmonyus.post-resource.get-by-code.timeout", defaultValue = "2s")
    Duration getByCodeTimeout;
    @ConfigProperty(name = "harmonyus.post-resource.create.timeout", defaultValue = "2s")
    Duration createTimeout;
    @ConfigProperty(name = "harmonyus.post-resource.modify.timeout", defaultValue = "2s")
    Duration modifyTimeout;
    @ConfigProperty(name = "harmonyus.post-resource.delete.timeout", defaultValue = "2s")
    Duration deleteTimeout;
    @ConfigProperty(name = "harmonyus.post-resource.get-all.max-retries", defaultValue = "1")
    long getAllMaxRetries;
    @ConfigProperty(name = "harmonyus.post-resource.get-by-code.max-retries", defaultValue = "1")
    long getByCodeMaxRetries;
    @ConfigProperty(name = "harmonyus.post-resource.create.max-retries", defaultValue = "1")
    long createMaxRetries;
    @ConfigProperty(name = "harmonyus.post-resource.modify.max-retries", defaultValue = "1")
    long modifyMaxRetries;
    @ConfigProperty(name = "harmonyus.post-resource.delete.max-retries", defaultValue = "1")
    long deleteMaxRetries;

    @Inject
    PostMapper mapper;

    @GET
    public Uni<List<Post>> getAll(@DefaultValue("") @QueryParam("authorCode") String authorCode,
                                  @DefaultValue("") @QueryParam("categoryCode") String categoryCode,
                                  @DefaultValue("") @QueryParam("tagCode") String tagCode) {
        Uni<List<PostEntity>> postEntitiesUni;
        Sort sortByCreationDateDescending = Sort.by("creation", Sort.Direction.Descending);
        if (!authorCode.isEmpty()) {
            postEntitiesUni = PostEntity.list("authorCode = ?1", sortByCreationDateDescending, authorCode);
        } else if (!categoryCode.isEmpty()) {
            postEntitiesUni = PostEntity.list("categoryCode = ?1", sortByCreationDateDescending, categoryCode);
        } else if (!tagCode.isEmpty()) {
            postEntitiesUni = PostEntity.list("tagsCode in ?1", sortByCreationDateDescending, tagCode);
        } else postEntitiesUni = PostEntity.listAll(sortByCreationDateDescending);
        return postEntitiesUni
                .ifNoItem().after(getAllTimeout).fail()
                .map(postEntities -> mapper.fromEntityList(postEntities))
                .onFailure().retry().atMost(getAllMaxRetries);
    }

    @GET
    @Path("/{code}")
    public Uni<Post> getByCode(@PathParam("code") String code) {
        Uni<PostEntity> postEntityUni = PostEntity.findById(code);
        return postEntityUni
                .ifNoItem().after(getByCodeTimeout).fail()
                .onItem().ifNull().failWith(NotFoundException::new)
                .map(mapper::fromEntity)
                .onFailure().retry().atMost(getByCodeMaxRetries);
    }

    @POST
    public Uni<Response> create(Post post) {
        PostEntity postEntity = new PostEntity();
        mapper.toEntity(postEntity, post);
        return postEntity.persist()
                .ifNoItem().after(createTimeout).fail()
                .map(unused -> Response.created(URI.create(String.format("/posts/%s", postEntity.getCode())))
                        .entity(mapper.fromEntity(postEntity)).build())
                .onFailure().retry().atMost(createMaxRetries);
    }

    @PUT
    @Path("/{code}")
    public Uni<Post> modify(@PathParam("code") String code, Post post) {
        Uni<PostEntity> postEntityUni = PostEntity.findById(code);
        return postEntityUni
                .ifNoItem().after(modifyTimeout).fail()
                .onItem().ifNull().failWith(NotFoundException::new)
                .flatMap(postEntity -> {
                    mapper.toEntity(postEntity, post);
                    return postEntity.persistOrUpdate().onItem().transform(unused -> postEntity);
                })
                .onItem().transform(postEntity -> mapper.fromEntity(postEntity))
                .onFailure().retry().atMost(modifyMaxRetries);
    }

    @DELETE
    @Path("/{code}")
    public Uni<Response> delete(@PathParam("code") String code) {
        Uni<PostEntity> postEntityUni = PostEntity.findById(code);
        return postEntityUni
                .ifNoItem().after(deleteTimeout).fail()
                .onItem().ifNull().failWith(NotFoundException::new)
                .flatMap(ReactivePanacheMongoEntityBase::delete)
                .map(unused -> Response.noContent().build())
                .onFailure().retry().atMost(deleteMaxRetries);
    }
}
