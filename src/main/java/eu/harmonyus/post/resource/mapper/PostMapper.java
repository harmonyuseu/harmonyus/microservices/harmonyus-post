package eu.harmonyus.post.resource.mapper;

import eu.harmonyus.post.model.PostEntity;
import eu.harmonyus.post.resource.model.Post;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class PostMapper {

    @ConfigProperty(name = "harmonyus.microservice.author-external-url")
    String authorUrl;
    @ConfigProperty(name = "harmonyus.microservice.category-external-url")
    String categoryUrl;
    @ConfigProperty(name = "harmonyus.microservice.tag-external-url")
    String tagUrl;

    public Post fromEntity(PostEntity postEntity) {
        Post post = new Post();
        post.body = postEntity.getBody();
        post.title = postEntity.getTitle();
        post.code = postEntity.getCode();
        if (postEntity.getCreation() != null) {
            post.creation = postEntity.getCreation().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        }
        if (postEntity.getModification() != null) {
            post.modification = postEntity.getModification().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        }
        post.author.url = authorUrl + "authors/" + postEntity.getAuthorCode();
        if (postEntity.getCategoryCode() != null) {
            post.category.url = categoryUrl + "categories/" + postEntity.getCategoryCode();
        }

        if (postEntity.getTagsCode() != null) {
            postEntity.getTagsCode().forEach(tagCode -> post.tags.add(new Post.UrlCode(tagUrl + "tags/" + tagCode)));
        }

        return post;
    }

    public List<Post> fromEntityList(List<PostEntity> postEntities) {
        List<Post> posts = new ArrayList<>();
        postEntities.forEach(postEntity -> posts.add(fromEntity(postEntity)));
        return posts;
    }

    public void toEntity(PostEntity postEntity, Post post) {
        if (post.code != null) {
            postEntity.setCode(post.code);
        }
        postEntity.setBody(post.body);
        postEntity.setTitle(post.title);
        if (post.creation != null) {
            postEntity.setCreation(LocalDateTime.parse(post.creation, DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        }
        if (post.modification != null) {
            postEntity.setModification(LocalDateTime.parse(post.modification, DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        }
        if (post.author != null) {
            postEntity.setAuthorCode(post.author.getCode());
        }
        if (post.category != null) {
            postEntity.setCategoryCode(post.category.getCode());
        }
        if (post.tags != null) {
            postEntity.setTagsCode(post.tags.stream().map(Post.UrlCode::getCode).collect(Collectors.toList()));
        }
    }
}
